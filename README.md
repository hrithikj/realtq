# realTQ

## Intro
**Task**: Setup a server which receives real-time streaming data from client, save it in a PostgreSQL and return an acknowledgment.

**Tech stack**: Golang for the server and JS Webpage for client.

## Setting up
### Libraries
Install the gorilla websocket library
`go get github.com/gorilla/websocket`

### Running 
Directly run the server
`go run main.go`
Head to: http://localhost:8080

## Architecture
`A more detailed architecture is in the write-up. This section is a brief overview of the task.`
The server and the client exchange real-time data using websockets. The Go server handles HTTP and WS requests. Due to lack of time, I've commented what would've been the ideal solution to save in DB- for now all received data is just printed on the console.

Transmitting is done using a web client which uses websockets to transmit the real-time data (in this case, the time and coordinates) and receive acknowledgements while updating front-end dynamically.