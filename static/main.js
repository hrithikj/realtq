const messages = document.querySelector('#messages')
const send = document.querySelector('#send')

const url = "ws://" + window.location.host + "/ws";
const ws = new WebSocket(url);

ws.onmessage = (msg) => {
    insertMessage(JSON.parse(msg.data));
};

send.onclick = () => {
    // Make shift coordinates
    const message = {
        time: Date().toString(),
        longitude: String(Date.now()/100000000000),
        latitude: String(Date.now()/100000000000),
    }

    ws.send(JSON.stringify(message));
};

insertMessage = (messageObj) => {
    const message = document.createElement('div');

    message.setAttribute('class', 'ack-message');
    // console.log("name: " + messageObj.username + " content: " + messageObj.message)
    message.textContent = messageObj;

    messages.appendChild(message);
    messages.insert(message, messages.firstChild);
}