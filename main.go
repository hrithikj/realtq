package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

var Clients = make(map[*websocket.Conn]bool)
var Broadcast = make(chan Data)
var upgrader = websocket.Upgrader{}

type Data struct {
	Time      string `json:"time"`
	Longitude string `json:"longitude"`
	Latitude  string `json:"latitude"`
}

func Home(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Server is listening.")
}

func handleMessages() {
	for {
		// Acquiring next message
		msg := <-Broadcast

		// Save in DB
		// dbErr := db.Save(msg)
		fmt.Println("Message Received: ", msg)

		// Sending Acknowledgements
		/*
			if dbErr != nil {
				ack := dbErr
			}
			else {
				ack := time.Now().String()
			}
		*/
		ack := time.Now().String()

		for Client := range Clients {
			err := Client.WriteJSON(ack)
			if err != nil {
				log.Printf("Error: %v", err)
				Client.Close()
				delete(Clients, Client)
			}
		}
	}
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrading connection to websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}

	defer ws.Close()

	Clients[ws] = true
	for {
		var msg Data

		// Reading message as JSON and mapping to Message
		err := ws.ReadJSON(&msg)
		if err != nil {
			log.Printf("Error: %v", err)
			delete(Clients, ws)
			break
		}

		// Send the message to Message Queue
		Broadcast <- msg
	}
}

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/home", Home)
	http.HandleFunc("/ws", handleConnections)

	// Monitoring incoming messages concurrently
	go handleMessages()

	log.Println("Starting server on PORT:8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
